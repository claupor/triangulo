
'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    return str(number)*number

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    cadena=""
    if number <10:
        try:
            for number in range (1, number+1):
                l= line(number)
                cadena=cadena+l+"\n"
        except ValueError:
            print("necesito un argumento :) ")
    return cadena

def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)

if __name__ == '__main__':
    main()

